#!/bin/bash
#
# System Transparency Integration Testsuite.

set -euo pipefail
cd "$(dirname "$0")"

################################################################################
# CONSTANTS: some defaults that may be interesting when hacking on st.

# stboot's kernel command line. used in build::stboot
declare -r stboot_cmdline="console=tty0,115200n8 console=ttyS0,115200n8"

# ospkg's kernel command line. used in build::ospkg
declare -r ospkg_cmdline="console=tty0 console=ttyS0,115200n8 rw rdinit=/lib/systemd/systemd"

# the initramfs cpio archive used as a base for the ospkg. used in build::ospkg
declare -r ospkg_initramfs="tmp/ubuntu-focal-amd64.cpio.gz"

# the kernel image packaged with the OS package. we don't touch it. used in
# build::ospkg
declare -r ospkg_vmlinuz="tmp/ubuntu-focal-amd64.vmlinuz"

# the URL prefix for the OS package archive URL in the OS package JSON
# descriptor. the archive's filename is appended to the URL. used in
# build::ospkg
declare -r ospkg_archive_url_prefix="http://10.0.2.2:8080/"

# path to the UEFI code and variable store. the variable store is used as a
# template and not modified.
declare -r ovmf_code=build/OVMF_CODE.fd
declare -r ovmf_vars=build/OVMF_VARS.fd

# the socket used to communicate with the software TPM and supervisord. used in services::*
declare -r swtpm_sock="$(pwd)/tmp/swtpm.sock"
declare -r supervisor_sock="$(pwd)/tmp/supervisor.sock"

# port used by the http server to serve the ospkg archives. passed to
# supervisord as and environment variable (see build/supervisord.conf)
declare -r http_port=8080

# the qemu command line. passed to supervisord as a environment variable (see
# build/supervisord.conf). add -enable-kvm will speed things up but will
# prevent the test suite from running in the CI containers.
#
# we forward ports a bunch of ports from localhost to the VM.
# - 2222: ssh, used in services::shutdown_qemu to shut down the VM. useful for
#         debugging. the ospkg accepts root login with the key in build/id_ed25519
# - 2009: port used by stprov to receive provisioning data.
# - 3001: port used by stauth to receive handle platform endorsement.
# - 3002: port used by stauth to receive quote requests (the remote attestation
#         protocol).
declare -r qemu_args="\
    -drive if=pflash,format=raw,readonly=on,file=${ovmf_code} \
    -drive if=pflash,format=raw,file=tmp/ovmf_vars.fd \
    -M q35 \
    -netdev user,id=net0,net=10.0.2.0/24,dhcpstart=10.0.2.15,dns=8.8.8.8,hostfwd=tcp::2222-:22,hostfwd=tcp::3001-:3001,hostfwd=tcp::3002-:3002,hostfwd=tcp::2009-:2009 \
    -device virtio-net-pci,netdev=net0,mac=52:54:00:12:34:56 \
    -object rng-random,filename=/dev/urandom,id=rng0 \
    -device virtio-rng-pci,rng=rng0 \
    -rtc base=localtime \
    -m 8G \
    -nographic \
    -drive file=tmp/stboot-prov.iso,format=raw,if=none,media=cdrom,id=drive-cd1,readonly=on \
    -device ahci,id=ahci0 \
    -device ide-cd,bus=ahci0.0,drive=drive-cd1,id=cd1,bootindex=1 \
    -chardev socket,id=chrtpm,path=${swtpm_sock} \
    -tpmdev emulator,id=tpm0,chardev=chrtpm \
    -device tpm-tis,tpmdev=tpm0 \
    -chardev socket,id=mon1,host=localhost,port=4444,server=on,wait=off \
    -mon chardev=mon1,mode=readline"


################################################################################
# BUILD FUNCTIONS: build the artifacts needed for the integration testsuite.

# stboot <output basename> <files>
#
# Build the stboot UKI and package it into an ISO using u-root. Requires the
# Linux kernel image, u-root and stmgr to be build already.
#
# The <files> argument are additional files that are packed into the initramfs
# for stboot. It is an associative array of <local path> => <initramfs path>,
# e.g. if you want to make the local file at build/foo available to stboot at
# /bin/foo you do
#
# > local -A files=( [build/foo]=bin/foo )
# > build::stboot "mystboot" files
#
# You should at lest include the trust policy and the root certificate that
# signed the OS packages and the host configuration.
#
# The function will create (overwrite) the following files:
# - tmp/<output basename>.iso: the stboot ISO image containing the UKI
# - tmp/<output basename>.stboot.pb: the stboot endorsement for remote attestation
function build::stboot {
  local -r output=$1
  local -n files_ref=$2
  local files_cmdline=""

  # add additional files to the initramfs, reformat as -files <src>:<dst>
  for src in "${!files_ref[@]}"; do
    local dst="${files_ref[$src]}"
    files_cmdline="${files_cmdline} -files ${src}:${dst}"
  done

  # build the linux boot initramfs w/ stboot included
  u-root/u-root \
    -build=bb \
    -uinitcmd=stboot \
    -defaultsh="" \
    ${files_cmdline} \
    -o tmp/stboot-initramfs.cpio \
    ./stboot ./u-root/cmds/core/init/

  rm -f tmp/stboot-initramfs.cpio.gz
  gzip -9 -n tmp/stboot-initramfs.cpio

  # build the stboot iso from the linux boot kernel and initramfs. stmgr does
  # not like it when the -out target already exists.
  rm -f "tmp/${output}.iso"
  stmgr/stmgr uki create \
    -format iso \
    -out "tmp/${output}.iso" \
    -kernel=tmp/linux-build/arch/x86/boot/bzImage \
    -initramfs=tmp/stboot-initramfs.cpio.gz \
    -cmdline "${stboot_cmdline}"

  stauth/stauth endorse --stboot "tmp/${output}.iso" -o tmp/${output}.stboot.pb
}

# ospkg <output basename> <files>
#
# Build the OS package that is booted by stboot either from network or
# initramfs. The function also is used to create the provisioning OS package
# for the "provision" scenario.
#
# The OS package kernel is build in a separate CI job:
# https://git.glasklar.is/system-transparency/core/example-os. This function
# adds additional files, re-packages the OS package and signs it using the keys
# in the build/ directory.
#
# The <files> argument are additional files that are packed into the initramfs
# of the OS package. It is an associative array of <local path> => <initramfs path>,
# e.g. if you want to make the local file at build/foo available to the OS
# package at /bin/foo you do
#
# > local -A files=( [build/foo]=bin/foo )
# > build::ospkg "tmp/myospkg" files
#
# If the <local path> is "nothing" the <initramfs path> is created as an empty
# file. Some care must be taken when adding files to directories that do not
# exist in the initramfs we use as a base (see ospkg_initramfs above).
# Directories must first me created by placing an empty .dir file in them:
#
# > local -A files=( [nothing]=root/.ssh/.dir [build/id_ed25519.pub]=root/.ssh/authorized_keys )
#
# Otherwise the initramfs will have messed up permissions. The .dir file will
# not be in the final initramfs.
#
# The function will create (overwrite) the following files:
# - <output basename>.zip: the OS package archive
# - <output basename>.json: the OS package JSON descriptor
# - <output basename>.ospkg.pb: the OS package endorsement
function build::ospkg {
  local -r output=$1
  local -n files_ref=$2
  local -r initramfs=$(mktemp)

  cp "${ospkg_initramfs}" "${initramfs}"

  # add additional files to the initramfs
  local -r cpio_dir=$(mktemp -d)

  # copy files to the cpio staging directory, special case for "nothing"
  for src in "${!files_ref[@]}"; do
    local dst="${files_ref[$src]}"
    mkdir -p "${cpio_dir}/$(dirname "${dst}")"

    case "${src}" in
      nothing)
        touch "${cpio_dir}/${dst}"
        ;;
      *)
        cp -d "${src}" "${cpio_dir}/${dst}"
        ;;
    esac
  done

  # directories first, in breadth-first order. If someone can figure out how to
  # do this without the .dir files, please do.
  (cd "${cpio_dir}"; find . -type f -name .dir -print | xargs dirname | tac | cpio --quiet -H newc --owner root:root -o | gzip -9 -n >> "${initramfs}")

  # then files and symlinks
  (cd "${cpio_dir}"; find . -type f -not -name .dir -print -o -type l -print | cpio --quiet -H newc --owner root:root -o | gzip -9 -n >> "${initramfs}")

  rm -rf "${cpio_dir}"

  local -r ospkg_zip="${output}.zip"
  local -r ospkg_json="${output}.json"
  local -r ospkg_pb="${output}.ospkg.pb"

  # build the ospkg
  stmgr/stmgr ospkg create \
    -out "tmp/${ospkg_zip}" \
    -label=example-ubuntu20 \
    -kernel=${ospkg_vmlinuz} \
    -initramfs="${initramfs}" \
    -cmdline="${ospkg_cmdline}" \
    "-url=${ospkg_archive_url_prefix}/${ospkg_zip}"

  rm -r "${initramfs}"

  # sign the ospkg with all keys in build/
  for key in build/signing-key-?.key; do
    stmgr/stmgr ospkg sign \
      "-key=$key" \
      "-cert=${key//\.key/\.pem}" \
      -ospkg "tmp/${ospkg_zip}"
  done

  # create the endorsement file for the OS package
  stauth/stauth \
    endorse \
    --ospkg-json "tmp/${ospkg_json}" \
    --ospkg-zip "tmp/${ospkg_zip}" \
    -o "tmp/${ospkg_pb}"
}

# build::linux
#
# Build the Linux kernel and fake-pmem driver. The kernel is only used for the
# stboot UKI. The fake-pmem driver is necessary for the stauth.
#
# The kernel is build using the config in build/linux-config. The object files
# are placed in tmp/linux-build.
function build::linux {
  # fake-pmem driver
  rm -rf linux/drivers/fake-pmem
  cp -r fake-pmem linux/drivers/fake-pmem
  echo "obj-\$(CONFIG_FAKE_PMEM) += fake-pmem/" >> linux/drivers/Makefile

  # build linux
  local -r build_dir=$(pwd)/tmp/linux-build
  mkdir -p "${build_dir}"
  cp build/linux-config "$build_dir/.config"
  (cd linux || exit; make O="$build_dir" "-j$(nproc)" olddefconfig bzImage)
}

# build::all
function build::all {
  # build the supporting tools. building go applications is so fast that we
  # don't bother with caching.
  (cd stmgr || exit; go build)
  (cd u-root || exit; go build)
  make -C stprov
  make -C stauth

  # patch and build linux boot kernel
  if [[ ! -f tmp/linux-build/arch/x86/boot/bzImage || "${STIT_REBUILD:-}" == "1" ]]; then
    build::linux
  fi

  # download kernel and initramfs used as a base for the ospkg
  if [[ ! -f tmp/ubuntu-focal-amd64.vmlinuz || "${STIT_REBUILD:-}" == "1" ]]; then
    mkdir -p tmp
    wget --continue https://git.glasklar.is/system-transparency/core/example-os/-/package_files/6/download -O tmp/ubuntu-focal-amd64.vmlinuz
    wget --continue https://git.glasklar.is/system-transparency/core/example-os/-/package_files/5/download -O tmp/ubuntu-focal-amd64.cpio.gz
  fi

  # build the ospkg for provisioning and regular boot, as well as stboot for
  # the different scenarios. all ospkgs contain

  # the provisioning ospkg contains stprov and a systemd service that runs it
  # on boot. see build/stprov.service
  if [[ ! -f tmp/prov-ospkg.zip || "${STIT_REBUILD:-}" == "1" ]]; then
    local -A prov_ospkg_files=(
      [nothing]=root/.ssh/.dir \
      [build/id_ed25519.pub]=root/.ssh/authorized_keys \
      [stprov/stprov]=usr/sbin/stprov \
      [build/stprov.service-link]=etc/systemd/system/multi-user.target.wants/stprov.service \
      [build/stprov.service]=etc/systemd/system/stprov.service )
    build::ospkg "prov-ospkg" prov_ospkg_files
  fi

  # the regular ospkg to test booting and remote attestation. it contains
  # stauth and a systemd service that runs it in quote mode on boot. see
  # build/stauth-quote.service
  if [[ ! -f tmp/run-ospkg.zip || "${STIT_REBUILD:-}" == "1" ]]; then
    local -A run_ospkg_files=(
      [nothing]=root/.ssh/.dir \
      [build/id_ed25519.pub]=root/.ssh/authorized_keys \
      [stauth/stauth]=usr/sbin/stauth \
      [build/stauth-quote.service-link]=etc/systemd/system/multi-user.target.wants/stauth-quote.service \
      [build/stauth-quote.service]=etc/systemd/system/stauth-quote.service )
    build::ospkg "run-ospkg" run_ospkg_files
  fi

  # the stboot iso for initramfs boot packages the ospkg at /ospkg/ospkg.*. see
  # build/host_configuration.json.initramfs and
  # build/trust_policy.json.initramfs
  # XXX: not used yet.
  if [[ ! -f tmp/stboot-initramfs.iso || "${STIT_REBUILD:-}" == "1" ]]; then
    local -A stboot_files_initramfs=(
      [build/trust_policy.json.local]=etc/trust_policy/trust_policy.json \
      [build/host_configuration.json.local]=etc/host_configuration.json \
      [build/root.pem]=etc/trust_policy/ospkg_signing_root.pem \
      [tmp/run-ospkg.zip]=ospkg/run-ospkg.zip \
      [tmp/run-ospkg.json]=ospkg/run-ospkg.json )
    build::stboot "stboot-initramfs" stboot_files_initramfs
  fi

  # the stboot iso for network boot. see build/host_configuration.json.net and
  # build/trust_policy.json.net
  # XXX: not used yet.
  if [[ ! -f tmp/stboot-net.iso || "${STIT_REBUILD:-}" == "1" ]]; then
    local -A stboot_files_net=(
      [build/trust_policy.json.net]=etc/trust_policy/trust_policy.json \
      [build/host_configuration.json.net]=etc/host_configuration.json \
      [build/root.pem]=etc/trust_policy/ospkg_signing_root.pem \
      [build/isrgrootx1.pem]=etc/ssl/certs/isrgrootx1.pem )
    build::stboot "stboot-net" stboot_files_net
  fi

  # the stboot iso for provisioning. it has no host configuration and includes
  # the provisioning ospkg at /ospkg/provision.* the trust policy is the
  # configured to net boot so that stboot starts a network boot after
  # provisioning.
  if [[ ! -f tmp/stboot-prov.iso || "${STIT_REBUILD:-}" == "1" ]]; then
    local -A stboot_files_prov=(
      [build/trust_policy.json.net]=etc/trust_policy/trust_policy.json \
      [build/root.pem]=etc/trust_policy/ospkg_signing_root.pem \
      [build/isrgrootx1.pem]=etc/ssl/certs/isrgrootx1.pem \
      [tmp/prov-ospkg.zip]=ospkg/provision.zip \
      [tmp/prov-ospkg.json]=ospkg/provision.json )
    build::stboot "stboot-prov" stboot_files_prov
  fi
}

################################################################################
# SERVICES: start and stop the services needed for the integration testsuite.

# the test suite requires juggling a lot of services. these functions help
# getting their status right. running the services is done using supervisord.
# the services are:
#
# - swtpm: the TPM simulator
# - http server: serves the ospkg archives
# - qemu: the VM

# services::wait <socket> <open/close> <name>
#
# waits for a socket (port or unix socket) to be open or closed. this is used
# to verify a particular service is available. supervisord only start and stops
# the application. services like swtpm need some time to become ready.
# additionally we use port to figure out when the ospkg inside the VM is done
# booting by waiting for SSH or another service to become available.
#
# the socket argument must be a valid address for socat(1) or be
# "HTTPS:<host>:<port>". in the latter case the function will use curl(1) to
# check if the port. the <open/close> argument is either "true" or "false" and
# indicates if the socket should be open or closed i.e. reachable or not. the
# <name> argument is used to print a message to the user about the progress.
function services::wait {
  local -r socket=$1
  local -r name=$3

  if [ "$2" == true ]; then
    echo -n "waiting for ${name} to start"
    if [[ "${socket}" =~ ^HTTPS: ]]; then
      while ! curl -s -m 2 -k https://${socket:6} > /dev/null; do
        echo -n "."
        sleep 1s
      done
    else
      while ! socat /dev/null "${socket},connect-timeout=2" 2>/dev/null; do
        echo -n "."
        sleep 1s
      done
    fi
  else
    echo -n "waiting for ${name} to stop"
    if [[ "${socket}" =~ ^HTTPS: ]]; then
      while curl -s -m 2 -k https://${socket:6} > /dev/null; do
        echo -n "."
        sleep 1s
      done
    else
      while socat /dev/null "${socket},connect-timeout=2" 2>/dev/null; do
        echo -n "."
        sleep 1s
      done
    fi
  fi

  echo ""
}

# services::start
#
# starts supervisord in the background and waits for the services to become
# ready. when the function returns the software TPM and http server are ready
# to be used. this function does not start qemu.
function services::start {
  # starting supervisor
  supervisord -c build/supervisord.conf
  services::wait "UNIX-CONNECT:${supervisor_sock}" true "supervisor"

  # wait for swtpm and http server to start
  echo -n "waiting for services to start"
  while : ; do
    local sv_status
    local running

    sv_status=$(supervisorctl \
      -c build/supervisord.conf \
      -s "unix://${supervisor_sock}" \
      status || true)
    running=$(echo "${sv_status}" | grep -c RUNNING || true)

    if [[ $running -eq 2 ]]; then
      break
    fi
    echo -n "."
    sleep 1s
  done
  echo ""

  # wait for swtpm to be ready
  services::wait "UNIX-CONNECT:${swtpm_sock}" true "swtpm"

  # wait for http server to be ready
  services::wait "TCP:127.0.0.1:${http_port}" true "http server"
}

# services::qemu <sense port>
#
# tells supervisord to start qemu and waits for it to become ready. supervisord
# needs to be started beforehand using services::start. when the function
# returns qemu is running and the HTTPS port $sense_port is open on localhost.
# the port is used to figure out when the VM is ready to be used. to correctly
# use this function the ospkg booted inside the VM must open the port when it
# is ready.
function services::qemu {
  local -r sense_port=${1:-2009}

  supervisorctl \
    -c build/supervisord.conf \
    -s "unix://${supervisor_sock}" \
    start qemu || true


  echo -n "waiting for qemu to start"
  while ! supervisorctl \
      -c build/supervisord.conf \
      -s "unix://${supervisor_sock}" \
      status qemu | grep -q RUNNING; do
  echo -n "."
    sleep 1s
  done
  echo ""

  # wait for qemu to be ready
  services::wait "TCP:127.0.0.1:4444" true "qemu monitor"

  # wait for qemu to boot into the vm
  services::wait "HTTPS:127.0.0.1:${sense_port}" true "VM"
}

# services::shutdown_qemu <wait only>
#
# shuts down the running VM using ssh and waits for qemu to stop. this function
# depends on the VM being reachable via ssh on port 2222 on localhost and
# permitting root login with the key in build/id_ed25519. if <wait only> is
# true the function will not shut down the VM using ssh but only wait for qemu
# to stop.
function services::shutdown_qemu {
  local -r wait_only=${1:-false}

  # connect to the VM using ssh and issue a poweroff
  if [[ $wait_only == false ]]; then
    echo "shutting down qemu"
    ssh \
      -o StrictHostKeyChecking=no \
      -o UserKnownHostsFile=/dev/null \
      -i build/id_ed25519 \
      -p 2222 \
      root@127.0.0.1 \
      systemctl poweroff
  fi

  # give the VM 10s to shut down. qemu will exit after that.
  local timeout=10
  echo -n "waiting for qemu to stop"
  while [[ $timeout -gt 1 ]]; do
    local running

    running=$(supervisorctl \
      -c build/supervisord.conf \
      -s "unix://${supervisor_sock}" \
      status qemu | grep -c RUNNING || true)

    if [[ $running -eq 0 ]]; then
      break
    fi

    echo -n "."
    let timeout=timeout-1 || true
    sleep 1s
  done

  # timeout reached, qemu did not stop. stop it forcefully.
  if [[ $timeout -eq 1 ]]; then
    echo "qemu did not stop, aborting"
    supervisorctl \
      -c build/supervisord.conf \
      -s "unix://${supervisor_sock}" \
      stop qemu

    # wait for supervisord to tell us qemu is stopped
    while : ; do
      local running

      running=$(supervisorctl \
        -c build/supervisord.conf \
        -s "unix://${supervisor_sock}" \
        status qemu | grep -c STOPPED || true)

      if [[ $running -eq 1 ]]; then
        break
      fi
      echo -n "."
      sleep 1s
    done
  fi
  echo ""
}

# services::stop
#
# shuts down all services started by services::start by shutting down
# supervisord. if qemu is still running it will be stopped too. when the
# function returns supervisord has exited.
function services::stop {
  supervisorctl \
    -c build/supervisord.conf \
    -s "unix://${supervisor_sock}" \
    shutdown

  services::wait "UNIX-CONNECT:${swtpm_sock}" false "supervisor"

  echo -n "waiting for supervisor to stop"
  while socat /dev/null "UNIX-CONNECT:${supervisor_sock},connect-timeout=2" 2>/dev/null; do
    echo -n "."
    sleep 1s
  done
  echo ""
}

################################################################################
# HELPER FUNCTIONS: functions that are used by the main function.


# set_efi_variable <name> <guid> <value> <fd file>
#
# adds or overwrites an EFI variable in the (emulated) NVRAM of the VM. we use
# this function to simulate provisioning.
# XXX: this function is not used yet. it's meant for simulating provisioning.
function set_efi_variable {
  local -r name="$1"
  local -r guid="$2"
  local -r value="$3"
  local -r file="$4"
  local -r newvar=$(mktemp)
  local -r newfile=$(mktemp)

  cat > "${newvar}" <<EOF
  {
    "version": 2,
    "variables": [{
      "name": "${name}",
      "guid": "${guid}",
      "attr": 7,
      "data": "${value}"
    }]
  }
EOF
  virt-fw-vars -i "${file}" -o "${newfile}" --set-json "${newvar}"
  mv "${newfile}" "${file}"
  rm "${newvar}"
}

# dependencies
#
# check we have all the tools we need to run the testsuite.
function dependencies {
  cmds=( qemu-system-x86_64 virt-fw-vars go make supervisord supervisorctl gcc wget gzip openssl socat xxd flex cpio ssh find )

  for cmd in "${cmds[@]}"; do
    if ! command -v "${cmd}" &> /dev/null; then
      echo "error: ${cmd} is not installed" >&2
      exit 1
    fi
  done
}

function main {
  # sanity check
  dependencies

  # build all artifacts
  build::all

  # environment variables passed to supervisord. these must be set before
  # calling services::* functions.
  export STIT_TMPDIR=$(pwd)/tmp
  export STIT_HTTP_PORT=${http_port}
  export STIT_QEMU_ARGS=${qemu_args}

  # start tpm simulator and http server
  services::start

  # in case things go wrong, shut down all services, including qemu
  trap "services::stop" ERR

  # create an empty set of EFI variables for the vm
  cp "${ovmf_vars}" tmp/ovmf_vars.fd

  # start qemu in the background. wait for the provisioning ospkg to boot. it
  # should start stprov on boot which will listen on port 2009.
  services::qemu 2009

  # run provisioning by connecting to stprov running inside the VM and
  ./stprov/stprov local run -i 127.0.0.1 -o "stboot"

  # restart qemu, expecting it to boot run-ospkg
  services::shutdown_qemu "true"
  services::qemu 3002

  # shutdown qemu and other services
  services::shutdown_qemu
  services::stop
}

main
