System Transparency Integration Test Suite
=========================================

This test suite consists of a bash script (`run.bash`) and supporting files
(`build/*`) that builds all components of System Transparency, provisions and a
platform for stboot and then boots an OS package. The script tests the
following scenario: start of a fresh VM, provision it for network boot using
stprov and then boot an OS package from the network.

Except for the UEFI running in the VM and the OS package to that is booted, all
artefacts are build from source. This includes u-root and Linux. The results are
cached in the `tmp` directory and are not rebuild if there are already present.
Rebuilding can be forced by setting `STIT_REBUILD=1` or simply deleting the
temporary directory.

The test suite uses submodules to select the right component version.

**To Do**: The script was meant to test different scenarios like network and
local boot using already provisioned VMs. Some of that is already implemented.
See comments in run.bash.
